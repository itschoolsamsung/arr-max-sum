public class Test {
    public static void printArr2(int[][] arr)
    {
        for (int[] r : arr) {
            int sum = 0;
            for (int c : r) {
                sum += c;
                System.out.print(c + " ");
            }
            System.out.println("= " + sum);
        }
    }

    public static void main(String[] args) {
        int[][] arr = new int[10][10];
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr[i].length; j++) {
                arr[i][j] = (int)(Math.random() * 10);
            }
        }
        printArr2(arr);
        int num = 0, maxSum = 0;
        for (int i = 0; i < arr.length; i++) {
            int sum = 0;
            for (int j = 0; j < arr[i].length; j++) {
                sum += arr[i][j];
            }
            if (sum > maxSum) {
                maxSum = sum;
                num = i;
            }
        }
        System.out.println(num);
    }
}
